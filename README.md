# HAE806E Practicals - ROS2 Python

## Introduction

In these practicals, you should learn the basics of **git** and **ROS2**. 

Find the assignments in the `assignement/` folder. 

## Requirements

- ROS2 IRON
- PlotJuggler
