# Practical 3

Develop a `high_level_manager` node complying with the following characteristics:

- Subscribed to a topic `/kin_data`, receiving messages with type `KinData`. This type should have the fields:
    - `float64 x`;
    - `float64 y`;
    - `float64[4] jacobian`;

- Subscribed to a topic `/launch` taking messages of type `std_msgs/msg/Bool`, such that when a `True` value is published, the whole process is launched;

- Publisher in a topic named `/ref_position`, in which a reference pose of type `RefPosition` is published in the following cases:
    - When a first message from `/kin_data` node is received, in order to initialize the `trajectory_generator`. After that, the manager should wait to the `launch` flag; 
    - When the streamed pose from `/kin_data` node is equal to the last reference pose sent;

- When created, the node should read the file `trajectories/test.csv` taking the sequence of reference positions.

- Note that no new pose should be sent when the last `RefPosition` is attained.

In summary, the expected result when running `src/planar_robot_python/bash_scripts/p2.sh` should be the following:

![](../gif/p3.gif)