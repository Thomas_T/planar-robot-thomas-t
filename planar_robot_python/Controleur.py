import rclpy
from rclpy.node import Node
from custom_msgs.msg import KinData, DesiredState
from sensor_msgs.msg import JointState

class Controleur(Node):

    def __init__(self):
        super().__init__('controleur_node')
        self.subscription_desired_state = self.create_subscription(
            DesiredState,
            '/desired_state',
            self.desired_state_callback,
            10)
        self.subscription_kin_data = self.create_subscription(
            KinData,
            '/kin_data',
            self.kin_data_callback,
            10)
        self.publisher_desired_joint_velocities = self.create_publisher(
            JointState,
            '/desired_joint_velocities',
            10)


        self.kin_data_received = False
        self.desired_cartesian_position = np.zeros(2)
        self.desired_cartesian_velocity = np.zeros(2)
        self.current_cartesian_position = np.zeros(2)
        self.jacobian = np.zeros([2,2])
        self.Kp                 = 2.0

    def desired_state_callback(self, msg):
        self.desired_cartesian_position.x = msg.x
        self.desired_cartesian_position.y = msg.y
        self.desired_cartesian_velocity.xdot = msg.xdot
        self.desired_cartesian_velocity.ydot = msg.ydot
        self.xd = np.array([msg.x , msg.y])
        self.x_dot = np.array([msg.xdot , msg.ydot])

    def _kin_data_callback(self, msg):
        self.kin_data_received = True
        self.current_cartesian_position.x = msg.x
        self.current_cartesian_position.y = msg.y 
        self.x = np.array([msg.x , msg.y])
        self.jacobian = msg.jacobian.reshape((2, 2))


    def publish_esired_joint_velocities(self):
        if self.current_cartesian_position is None and self.Jacobian is None:

            desired_joint_velocities = JointState()
            desired_joint_velocities.velocity = np.zeros(2)

        else:
            current_vel = np.zeros(2)
            self.erreur         = self.desired_cartesian_position-self.current_cartesian_position # xd-x
            self.current_vel    = self.Kp*self.erreur #multiply by gain
            self.dot_erreur     = self.current_vel+self.desired_cartesian_velocity

            desired_joint_velocities = JointState()
            desired_joint_velocities.velocity = np.matmul(np.linalg.pinv(self.Jacobian),self.dot_erreur)

        self.publisher_.publish(desired_joint_velocities)

def main(args=None):
    rclpy.init(args=args)
    controleur = Controleur()
    rclpy.spin(controleur)
    controleur.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
