import rclpy
from geometry_msgs.msg import RefPosition, CartesianState
from trajectory_msgs.msg import PositionInterpolation


class TrajectoryGenerator:

    def __init__(self):
        # Attributs
        self._node = rclpy.create_node("trajectory_generator")
        self._ref_position_sub = self._node.create_subscription(
            RefPosition, "/ref_position", self._ref_position_callback
        )
        self._desired_state_pub = self._node.create_publisher(
            CartesianState, "/desired_state"
        )
        self._current_position = None
        self._position_interpolator = PositionInterpolation(
            [1.0, 2.0], [3.0, 4.0], 2.0
        )

    def _ref_position_callback(self, msg):
        # Si c'est le premier message, initialiser la position courante
        if self._current_position is None:
            self._current_position = msg

        # Calculer la trajectoire et diffuser l'état désiré
        desired_state = CartesianState()
        t = self._position_interpolator.get_time_from_position(
            self._current_position.x, self._current_position.y
        )
        pos = self._position_interpolator.get_position(t)
        desired_state.x = pos[0]
        desired_state.y = pos[1]
        self._desired_state_pub.publish(desired_state)

        # Mettre à jour la position courante pour la prochaine trajectoire
        self._current_position = msg

def main():
    rclpy.init()
    node = TrajectoryGenerator()
    rclpy.spin(node)

if __name__ == "__main__":
    main()
