import rclpy
from rclpy.node import Node
from customs_messages.msg import RefPosition, KinData
from std_msgs.msg import Bool
from trajectory_generator import Trajectory_generator
import csv
import os

class HighLevelManager(Node):
	def __init__(self):
		super().__init__('high_level_manager')
		self._node = rclpy.create_node("high_level_manager")
        self._kin_data_sub = self.create_subscription(KinData,"/kin_data",self._kin_data_callback, 10)
        self._launch_sub = self.create_subscription(Bool, 
            "/launch", 
        	self._launch_callback, 
            10)
        self._ref_position_pub = self.create_publisher(
            RefPosition, 
            "/ref_position",
            10)
        self.initialized = False
        self.trajectory_generator = Trajectory_generator()
        self.trajectory = []
        self.last_ref_position = None
        self.read_trajectory_from_file()

    def read_trajectory_from_file(self):
        with open('/home/serroukha/rw2/src/planar_robot_thomas/trajectoriestest.csv', 'r') as file:
            reader = csv.reader(file)
            next(reader) # Skip the header
            for row in reader:
                x, y, *_ = map(float, row) # Convert to float and store
                self.trajectory.append((x, y))

    def _kin_data_callback(self, msg):
        if not self.initialized:
            self.initialized = True
            self._trajectory_generator.initialize(msg.x, msg.y)

        if self._last_ref_position is None or msg.x == self._last_ref_position.x and msg.y == self._last_ref_position.y:
            self._last_ref_position = self._trajectory_generator.get_next_position()
            if self._last_ref_position is not None:
                self.self.send_reference_pose()


    def _launch_callback(self, msg):
        if msg.data:
            self._trajectory_generator.start()

    def send_reference_pose(self):
        if self.trajectory:
            x, y = self.trajectory[0]
            ref_position_msg = RefPosition()
            ref_position_msg.x = x
            ref_position_msg.y = y
            self.publisher_.publish(ref_position_msg)
            self.trajectory.pop(0)


def main():
    rclpy.init(args=args)
    High_Level_Manager = HighLevelManager()
    rclpy.spin(High_Level_Manager)
    High_Level_Manager.destroy_node()
    rclpy.shutdown()

if __name__ == "__main__":
    main()

