import rclpy
from rclpy.node import Node
from custom_messages.msg import CartesianState
from custom_messages.msg import RefPosition
from std_msgs.msg import String
import numpy as np

class Trajectory_generator(Node):
    def __init__(self):
        super().__init__('trajectory_generator')
        self.subscription = self.create_subscription(
            RefPosition,
            '/ref_position',
            self._ref_position_callback,
            10)
        self.publisher_ = self.create_publisher(CartesianState,
            '/desired_state',10)
        self.subscription
        self._current_position = None
        self._position_interpolator = PositionInterpolation(
            [1.0, 2.0], [3.0, 4.0], 2.0)

    def _ref_position_callback(self):
        # initialiser la postion courante
        if self._current_position is None :
            self._current_position = std_msg
        
        #calcul de la trajectoire
        desired_state = CartesianState()
        t = self._position_interpolator.get_time_from_position(
            self._current_position.x, self._current_position.y
        )
        pos = self._position_interpolator.get_position(t)
        desired_state.x = pos[0]
        desired_state.y = pos[1]
        self.publisher_.publish(desired_state)


def main(args=None):
    rclpy.init(args=args)
    trajectory_generator = Trajectory_generator()
    rclpy.spin(trajectory_generator)
    trajectory_generator.destroy_node()
    rclpy.shutdown()


    
if __name__ == '__main__':
    main()



