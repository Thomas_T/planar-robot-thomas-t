import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np


class Simulateur(Node):

    def __init__(self):
        # Attributs     
        self.subscription_desired_joint_velocities = self.create_subscription(
            JointState,
            '/desired_joint_velocities',
            self.desired_joint_velocities_callback,
            10)
        self.publisher_joint_states = self.create_publisher(
            JointState,
            '/joint_states',
            10)
        self._joint_state = JointState()
        self.joint_positions = np.zeros(2)  # positions acticulaire
        self.dt = 0.01  # pas de temps

    def _desired_joint_velocities_callback(self, msg):
        # Mettre à jour les vitesses articulaires dans l'attribut _joint_state
        self.desired_joint_velocities = np.array(msg.velocity)
            
    def publish_joint_states(self):
        # Intégrer les vitesses articulaires pour obtenir les positions articulaires simulées
        self.joint_positions += self.desired_joint_velocities * self.dt

        # Publier les positions articulaires simulées sur le sujet /joint_states
        
        joint_states_msg = JointState()
        joint_states_msg.position = self.joint_positions
        self.publisher_joint_states.publish(joint_states_msg)

def main():
    rclpy.init(args=args)
    simulateur = Simulateur()
    rclpy.spin(simulateur)
    simulateur.destroy_node()
    rclpy.shutdown()

if __name__ == "__main__":
    main()
